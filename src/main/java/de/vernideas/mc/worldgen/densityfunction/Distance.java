package de.vernideas.mc.worldgen.densityfunction;

import com.mojang.serialization.Codec;
import com.mojang.serialization.MapCodec;

import com.mojang.serialization.codecs.RecordCodecBuilder;
import net.minecraft.util.dynamic.CodecHolder;
import net.minecraft.world.gen.densityfunction.DensityFunction;

/**
 * Density function calculating Euclidean distance from the origin (X=0, Z=0) and returning a value of
 * 0 when the maximum value is reached or exceeded, and 1 at the origin itself. The values inbetween
 * are linearly interpolated. The Y-value (height) is ignored.
 * <br><br>
 * <strong>ID:</strong><code>"akj_worldgen:distance"</code>
 * <br><br>
 * Parameter:
 * <dl>
 * <dt>"argument"</dt><dd>Data type: double<br>
 *   Value range: 1.0 to 1000000000.0<br>
 *   The distance at and beyond which this function returns 0.0</dd>
 * <dt>Return value</dt><dd>1.0 exactly at the origin (X=0, Z=0).<br>
 *   0.0 exactly at the distance specified in "argument" and beyond it.<br>
 *   Linearly in respect to distance interpolated value between those two.</dd>
 * </dl>
 * <br><br>
 * Example:
 * <pre>
 * {
 *  "type": "akj_worldgen:distance",
 *  "argument": 1000
 * }
 * </pre>
 * This returns a value of 0.0 at and beyond 1000 blocks distance from the origin,
 * and above it (up to 1.0) the closer you sample it to the origin.
 * <br><br>
 * See also: <code>"minecraft:y_clamped_gradient"</code> for a similar function, but for the Y coordinate only.
 */
public record Distance(double min, double max) implements DensityFunction.Base {
	public static final MapCodec<Distance> DISTANCE_CODEC = RecordCodecBuilder.mapCodec(
			(instance) -> instance.group(
					(Codec.doubleRange(0.0,1000000000.0).fieldOf("min")).forGetter(Distance::min),
					(Codec.doubleRange(1.0,1000000000.0).fieldOf("max")).forGetter(Distance::max)).apply(instance, Distance::new));
	private static final CodecHolder<? extends DensityFunction> CODEC_HOLDER = CodecHolder.of(DISTANCE_CODEC);
	
	@Override public CodecHolder<? extends DensityFunction> getCodecHolder() {
		return CODEC_HOLDER;
	}

	@Override public double maxValue() {
		return 1.0;
	}

	@Override public double minValue() {
		return 0.0;
	}

	@Override public double sample(NoisePos pos) {
		double dist = Math.sqrt(1.0 * pos.blockX() * pos.blockX() + 1.0 * pos.blockZ() * pos.blockZ());
		return dist > max ? 0.0 : (dist < min ? 1.0 : (max - dist) / (max - min));
	}

}
